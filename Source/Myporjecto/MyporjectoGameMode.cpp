// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyporjectoGameMode.h"
#include "MyporjectoCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyporjectoGameMode::AMyporjectoGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
