// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ZombieDR.generated.h"


//Data row de los enemigos

UENUM(BlueprintType)
enum FEZombieType
{
	BASIC,
	WEAK,
	STRONG
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct MYPORJECTO_API FZombieDR : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<FEZombieType> Zombie;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int InitialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int InitialDmg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int InitialVelvet;
};
