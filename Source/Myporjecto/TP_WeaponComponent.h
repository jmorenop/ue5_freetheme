// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TP_WeaponComponent.generated.h"

class AMyporjectoCharacter;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPORJECTO_API UTP_WeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	//referencia a sí mismo
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UTP_PickUpComponent* myActor;
	
	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AMyporjectoProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;
	
	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector MuzzleOffset;

	//propiedas del arma, como la munición, si está recargando o disparando
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	int ammo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	int ammoMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	bool isReloading;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	float reloadTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	bool isShooting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	float shootingTime;

	FTimerHandle th;


public:

	/** Sets default values for this component's properties */
	UTP_WeaponComponent();

	/** Attaches the actor to a FirstPersonCharacter */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void AttachWeapon(AMyporjectoCharacter* TargetCharacter);

	//Habilita el arma
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void Enable();

	//Deshabilita el arma
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void Disable();

	/** Make the weapon Fire a Projectile */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void Fire();

	//Acabar de recargar
	void endReload();

	//Acabar de disparar
	void endShooting();

protected:
	/** Ends gameplay for this component. */
	UFUNCTION()
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
		

private:
	/** The Character holding this weapon*/
	AMyporjectoCharacter* Character;
};
