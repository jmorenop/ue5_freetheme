// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hitteable.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

UCLASS()
class MYPORJECTO_API AEnemy : public APawn, public IHitteable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Hace x de daño al enemigo 
	virtual void Hit_Implementation(int dmg) override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
