// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "MasterDataTable.generated.h"

UENUM(BlueprintType)
enum EClases
{
	BASIC,
	WEAK,
	STRONG
};

/**
 * 
 */

USTRUCT()
struct MYPORJECTO_API FMasterDataTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> zombie;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialDmg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialVel;

	FMasterDataTable() : zombie(BASIC), initialHp(100), initialDmg(25), initialVel(50) {}
	
};

