// Copyright Epic Games, Inc. All Rights Reserved.


#include "TP_WeaponComponent.h"
#include "MyporjectoCharacter.h"
#include "MyporjectoProjectile.h"
#include "TP_PickUpComponent.h"
#include "GameFramework/PlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "Field/FieldSystemNodes.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UTP_WeaponComponent::UTP_WeaponComponent()
{
	// Default offset from the character location for projectiles to spawn
	MuzzleOffset = FVector(100.0f, 0.0f, 10.0f);
	ammo = ammoMax;
}


void UTP_WeaponComponent::Fire()
{
	if (Character == nullptr || Character->GetController() == nullptr)
		return;

	//if ammo is 0 or below AND isnt reloading, then reload
	if (ammo <= 0 && !isReloading)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Recargando esperate"));		
		isReloading = true;
		GetWorld()->GetTimerManager().SetTimer(th, this, &UTP_WeaponComponent::endReload, reloadTime, false);
		return;
	}

	//if is reloading or shooting, return
	if (isReloading || isShooting)
		return;

	shootingTime = true;
	ammo--;
	
	GetWorld()->GetTimerManager().SetTimer(th, this, &UTP_WeaponComponent::endShooting, shootingTime, false);

	// Try and fire a projectile
	if (ProjectileClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
			const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = GetOwner()->GetActorLocation() + SpawnRotation.RotateVector(MuzzleOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride =
				ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
			ActorSpawnParams.Instigator = Character;

			// Spawn the projectile at the muzzle
			World->SpawnActor<AMyporjectoProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}

	// Try and play the sound if specified
	if (FireSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, Character->GetActorLocation());
	}

	// Try and play a firing animation if specified
	if (FireAnimation != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Character->GetMesh1P()->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void UTP_WeaponComponent::endReload()
{
	ammo = ammoMax;
	isReloading = false;
}

void UTP_WeaponComponent::endShooting()
{
	isShooting = false;
}

void UTP_WeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (Character != nullptr)
	{
		// Unregister from the OnUseItem Event
		Character->OnUseItem.RemoveDynamic(this, &UTP_WeaponComponent::Fire);
	}
}

void UTP_WeaponComponent::AttachWeapon(AMyporjectoCharacter* TargetCharacter)
{
	Character = TargetCharacter;
	if (Character != nullptr)
	{
		// Attach the weapon to the First Person Character
		FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
		GetOwner()->AttachToComponent(Character->GetMesh1P(), AttachmentRules, FName(TEXT("GripPoint")));

		// Register so that Fire is called every time the character tries to use the item being held
		Character->OnUseItem.AddDynamic(this, &UTP_WeaponComponent::Fire);
	}
}

void UTP_WeaponComponent::Enable()
{
	if (Character != nullptr)
	{
		Character->OnUseItem.AddDynamic(this, &UTP_WeaponComponent::Fire);
		if (myActor)
		{
			UE_LOG(LogTemp, Warning, TEXT("Ratón muerto %s"), *myActor->selfActor->GetName());
			myActor->mMesh->SetVisibility(true);
		}
	}
}

void UTP_WeaponComponent::Disable()
{
	if (Character != nullptr)
	{
		Character->OnUseItem.RemoveDynamic(this, &UTP_WeaponComponent::Fire);
		if (myActor)
		{
			UE_LOG(LogTemp, Warning, TEXT("Ayayay %s"), *myActor->selfActor->GetName());
			myActor->mMesh->SetVisibility(false);
			if (myActor->mMesh)
				UE_LOG(LogTemp, Warning, TEXT("2222222222222 %s"), *myActor->selfActor->GetName());
		}
	}
}
