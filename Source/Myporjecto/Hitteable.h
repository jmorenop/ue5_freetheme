// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Hitteable.generated.h"

/**
 * 
 */

UINTERFACE(MinimalAPI)
class UHitteable : public UInterface
{
	GENERATED_BODY()
};



class MYPORJECTO_API IHitteable
{
	GENERATED_BODY()
	
public:
	int initialHp = 20;
	int hp = 20;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Hit(int dmg);
};
